## Run app in docker
#### 1. Create network
```sh
$ docker network create oneportal-news
```
#### 2. Run Elasticsearch
```sh
$ docker run -d --name elastic -p 9200:9200 -p 9300:9300 --network oneportal-news -e "discovery.type=single-node" elasticsearch:7.6.1
```
#### 3. Run app 
```sh
$ docker run -d --name oneportal-news -p 8080:8080 --network oneportal-news --env config=config-dev.json *IMAGE_ID*
```
