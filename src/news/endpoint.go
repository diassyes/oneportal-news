package news

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"oneportal-news/src/domain"
)

// -------------------- SAVE POST (request, response, endpoint) --------------------
type savePostRequest struct {
	domain.Post
}

type savePostResponse struct {
	Uid string `json:"uid"`
	Msg string `json:"msg"`
}

func makeSavePostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(savePostRequest)
		resp, err := s.SavePost(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}

// -------------------- GET POSTS (request, response, endpoint) --------------------
type getPostsRequest struct {
	Size int `json:"size"`
	From int `json:"from"`
}

type getPostsResponse struct {
	AllPostsQty int64         `json:"all_posts_quantity"`
	Posts       []domain.Post `json:"posts"`
}

func makeGetPostsEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPostsRequest)
		resp, err := s.GetPosts(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}

// -------------------- EDIT POST (request, response, endpoint) --------------------
type editPostRequest struct {
	domain.Post
}

type editPostResponse struct {
	Msg string `json:"msg"`
}

func makeEditPostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(editPostRequest)
		resp, err := s.EditPost(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}

// -------------------- GET POST (request, response, endpoint) --------------------
type getPostRequest struct {
	Uid string `json:"uid"`
}

type getPostResponse struct {
	domain.Post
}

func makeGetPostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPostRequest)
		resp, err := s.GetPost(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}

// -------------------- DELETE POST (request, response, endpoint) --------------------
type deletePostRequest struct {
	Uid string `json:"uid"`
}

type deletePostResponse struct {
	Msg string `json:"msg"`
}

func makeDeletePostEndpoint(s Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deletePostRequest)
		resp, err := s.DeletePost(&req)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}
}
