package news

import (
	"github.com/go-kit/kit/log"
	"time"
)

// Additional structures
type loggingService struct {
	logger log.Logger
	Service
}

// Additional functions
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

// ------------------------- Logging functions -------------------------

// Create/Update post logging service
func (s *loggingService) SavePost(req *savePostRequest) (_ *savePostResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "save_post",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.SavePost(req)
}

// Get posts logging service
func (s *loggingService) GetPosts(req *getPostsRequest) (_ *getPostsResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "get_posts",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.GetPosts(req)
}

// Get post logging service
func (s *loggingService) GetPost(req *getPostRequest) (_ *getPostResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "get_post",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.GetPost(req)
}

// Edit post logging service
func (s *loggingService) EditPost(req *editPostRequest) (_ *editPostResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "edit_post",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.EditPost(req)
}

// Delete post logging service
func (s *loggingService) DeletePost(req *deletePostRequest) (_ *deletePostResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
			_ = s.logger.Log(
				"method", "delete_post",
				"took", time.Since(begin),
				"err", err,
			)
		}
	}(time.Now())

	return s.Service.DeletePost(req)
}