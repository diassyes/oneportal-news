package news

import (
	"github.com/go-kit/kit/metrics"
	"time"
)

// Additional structures
type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	requestError   metrics.Counter
	Service
}

// Additional functions
func NewInstrumentingService(counter metrics.Counter, latency metrics.Histogram, counterE metrics.Counter, s Service) Service {
	return &instrumentingService{
		requestCount:   counter,
		requestLatency: latency,
		requestError:   counterE,
		Service:        s,
	}
}

// ------------------------- Instrumenting functions -------------------------

// Create/Update post instrumenting service
func (s *instrumentingService) SavePost(req *savePostRequest) (_ *savePostResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "save_post").Add(1)
		if err != nil {
			s.requestError.With("method", "save_post").Add(1)
		}
		s.requestLatency.With("method", "save_post").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SavePost(req)
}

// Get posts instrumenting service
func (s *instrumentingService) GetPosts(req *getPostsRequest) (_ *getPostsResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "get_posts").Add(1)
		if err != nil {
			s.requestError.With("method", "get_posts").Add(1)
		}
		s.requestLatency.With("method", "get_posts").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetPosts(	req)
}

// Get post instrumenting service
func (s *instrumentingService) GetPost(req *getPostRequest) (_ *getPostResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "get_post").Add(1)
		if err != nil {
			s.requestError.With("method", "get_post").Add(1)
		}
		s.requestLatency.With("method", "get_post").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetPost(req)
}

// Edit post instrumenting service
func (s *instrumentingService) EditPost(req *editPostRequest) (_ *editPostResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "edit_post").Add(1)
		if err != nil {
			s.requestError.With("method", "edit_post").Add(1)
		}
		s.requestLatency.With("method", "edit_post").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.EditPost(req)
}

// Delete post instrumenting service
func (s *instrumentingService) DeletePost(req *deletePostRequest) (_ *deletePostResponse, err error) {
	defer func(begin time.Time) {
		s.requestCount.With("method", "delete_post").Add(1)
		if err != nil {
			s.requestError.With("method", "delete_post").Add(1)
		}
		s.requestLatency.With("method", "delete_post").Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DeletePost(req)
}