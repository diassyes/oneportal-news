package news

import (
	"context"
	"encoding/json"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"net/http"
	"oneportal-news/src/errors"
)

func MakeHandler(s Service) http.Handler {

	// Options array
	options := []kithttp.ServerOption{
		kithttp.ServerErrorEncoder(encodeError),
	}
	// News routes handlers
	savePost := kithttp.NewServer(
		makeSavePostEndpoint(s),
		decodeSavePostRequest,
		encodeResponse,
		options...,
	)
	getPosts := kithttp.NewServer(
		makeGetPostsEndpoint(s),
		decodeGetPostsRequest,
		encodeResponse,
		options...,
	)
	editPost := kithttp.NewServer(
		makeEditPostEndpoint(s),
		decodeEditPostRequest,
		encodeResponse,
		options...,
	)
	getPost := kithttp.NewServer(
		makeGetPostEndpoint(s),
		decodeGetPostRequest,
		encodeResponse,
		options...,
	)
	deletePost := kithttp.NewServer(
		makeDeletePostEndpoint(s),
		decodeDeletePostRequest,
		encodeResponse,
		options...,
	)

	// Init router
	r := mux.NewRouter()

	// News routes
	r.Handle("/news/post", savePost).Methods("POST")
	r.Handle("/news/posts", getPosts).Methods("POST")
	r.Handle("/news/post/{uid}", editPost).Methods("PUT")
	r.Handle("/news/post/{uid}", getPost).Methods("GET")
	r.Handle("/news/post/{uid}", deletePost).Methods("DELETE")

	return r
}

// ----------------------- SAVE POST (request decoder) -----------------------
func decodeSavePostRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body savePostRequest

	// Parse request
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	// Validate request
	if body.Title == "" {
		return nil, errors.InvalidCharacter.DevMessage("post title must be provided")
	}
	if body.Content == "" {
		return nil, errors.InvalidCharacter.DevMessage("post content must be provided")
	}

	return body, nil
}

// ----------------------- GET POSTS (request decoder) -----------------------
func decodeGetPostsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body getPostsRequest

	// Parse request
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	return body, nil
}

// ----------------------- EDIT POST (request decoder) -----------------------
func decodeEditPostRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var body editPostRequest

	// Parse request
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		return nil, errors.InvalidCharacter.DevMessage(err.Error())
	}

	vars := mux.Vars(r)
	uid, ok := vars["uid"]
	if !ok {
		return nil, errors.NoFound
	}

	if body.Uid != "" {
		return nil, errors.AccessDenied.DevMessage("post uid can't be edited")
	}
	if body.CreatedOn != nil {
		return nil, errors.AccessDenied.DevMessage("post created on date can't be edited NEW")
	}
	if body.UpdatedOn != nil {
		return nil, errors.AccessDenied.DevMessage("post update on date can't be edited")
	}

	body.Uid = uid

	return body, nil
}

// ----------------------- GET POST (request decoder) -----------------------
func decodeGetPostRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req getPostRequest
	vars := mux.Vars(r)

	uid, ok := vars["uid"]
	if !ok {
		return nil, errors.NoFound
	}

	req.Uid = uid

	return req, nil
}

// ----------------------- DELETE POST (request decoder) -----------------------
func decodeDeletePostRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req deletePostRequest
	vars := mux.Vars(r)

	uid, ok := vars["uid"]
	if !ok {
		return nil, errors.NoFound
	}

	req.Uid = uid
	return req, nil
}

// --------------------------------------------------------------------------
// Response encoders
func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	e, ok := response.(errorer)
	if ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

// Error response encoder
type errorer interface {
	error() error
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	e, ok := err.(*errors.ArgError)
	if ok {
		w.WriteHeader(e.Status)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	_ = json.NewEncoder(w).Encode(err)
}
