package news

import (
	"oneportal-news/src/domain"
	"time"
)

type Service interface {
	SavePost(req *savePostRequest) (*savePostResponse, error)
	GetPost(req *getPostRequest) (*getPostResponse, error)
	GetPosts(req *getPostsRequest) (*getPostsResponse, error)
	EditPost(req *editPostRequest) (*editPostResponse, error)
	DeletePost(req *deletePostRequest) (*deletePostResponse, error)
}

type service struct {
	postCommandRepo domain.PostCommandRepo
}

func NewService(postCR domain.PostCommandRepo) Service {
	return &service{postCR}
}

func (s *service) SavePost(req *savePostRequest) (*savePostResponse, error) {
	post := req.Post

	if post.Uid == "" {
		post.GenerateUid()
	}

	timeNowUtc := time.Now().UTC()
	post.CreatedOn = &timeNowUtc

	err := s.postCommandRepo.Store(&post)
	if err != nil {
		return nil, err
	}

	return &savePostResponse{post.Uid, "post saved successfully"}, nil
}

func (s *service) GetPosts(req *getPostsRequest) (*getPostsResponse, error) {
	posts, allPostsQty, err := s.postCommandRepo.GetPostsSortedByCreatedOnAndAllPostsQty(req.Size, req.From)
	if err != nil {
		return nil, err
	}
	return &getPostsResponse{allPostsQty, *posts}, nil
}

func (s *service) EditPost(req *editPostRequest) (*editPostResponse, error) {
	post := req.Post

	timeNowUtc := time.Now().UTC()
	post.UpdatedOn = &timeNowUtc

	err := s.postCommandRepo.Edit(&post)
	if err != nil {
		return nil, err
	}
	return &editPostResponse{"post edited successfully"}, nil
}

func (s *service) GetPost(req *getPostRequest) (*getPostResponse, error) {
	post, err := s.postCommandRepo.GetByUid(req.Uid)
	if err != nil {
		return nil, err
	}
	return &getPostResponse{*post}, nil
}

func (s *service) DeletePost(req *deletePostRequest) (*deletePostResponse, error) {
	err := s.postCommandRepo.DeleteByUid(req.Uid)
	if err != nil {
		return nil, err
	}
	return &deletePostResponse{"post deleted successfully"}, nil
}
