package config

import (
	"encoding/json"
	"os"
)

// Config structures & vars
type Configs struct {
	Elastic  ElasticConfig  `json:"elastic"`
}

type ElasticConfig struct {
	ConnectionUrl []string `json:"connection_url"`
}

// Get configs
func GetConfigs() (*Configs, error) {

	var filePath string
	var configs Configs

	currentDir, err := os.Getwd()
	if err != nil {
		return nil, err
	}
	if last := len(currentDir) - 1; currentDir[last] == '/' {
		currentDir = currentDir[:last]
	}
	if os.Getenv("config") != "" {
		filePath = currentDir + "/src/config/" + os.Getenv("config")
	} else {
		filePath = currentDir + "/src/config/config.json"
	}

	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&configs)
	if err != nil {
		return nil, err
	}

	return &configs, nil
}
