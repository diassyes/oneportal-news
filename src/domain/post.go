package domain

import (
	"github.com/google/uuid"
	"time"
)

type Post struct {
	Uid              string     `json:"uid,omitempty"`
	Title            string     `json:"title,omitempty"`
	ShortDescription string     `json:"short_description,omitempty"`
	Content          string     `json:"content,omitempty"`
	Tags             []string   `json:"tags,omitempty"`
	Files            []string   `json:"files,omitempty"`
	CreatedOn        *time.Time `json:"created_on,omitempty"`
	UpdatedOn        *time.Time `json:"updated_on,omitempty"`
}

// Generate post unique ID
func (p *Post) GenerateUid() {
	p.Uid = uuid.New().String()
}

// Repository interfaces
type PostCommandRepo interface {
	Store(post *Post) error
	GetPostsSortedByCreatedOnAndAllPostsQty(size int, from int) (*[]Post, int64, error)
	Edit(post *Post) error
	GetByUid(id string) (*Post, error)
	DeleteByUid(uid string) error
}
