package errors

import "fmt"

// Errors variables
var (
	// 200 series errors
	NoContentFound = &ArgError{"ONELAB", 204, 3, "ONETECH.20403001", "Ничего не найдено", "Ничего не найдено", "docs/ONETECH.20403001"}

	// 400 series errors
	InvalidCharacter  = &ArgError{"ONETECH", 400, 3, "ONETECH.40003001", "Неправильные входные данные", "Неправильный JSON", "docs/ONETECH.40003001"}
	AccessDenied      = &ArgError{"ONETECH", 403, 3, "ONETECH.40303001", "Доступ к ресурсу запрещен", "Доступ к ресурсу или отдельной его части запрещен", "docs/ONETECH.40303001"}
	NoFound           = &ArgError{"ONETECH", 404, 3, "ONETECH.40403002", "Ресурс не найден", "Нет такого микросервиса", "docs/ONETECH.40403002"}
	Conflict          = &ArgError{"ONETECH", 409, 3, "ONETECH.40903001", "Конфликт обращения к ресурсу", "Запрос не может быть выполнен из-за конфликтного обращения к ресурсу", "docs/ONETECH.40903001"}
	ScyllaDBSaveError = &ArgError{"ONETECH", 409, 3, "ONETECH.40903002", "Ошибка записи в базу", "Недоступна база", "docs/ONETECH.40903002"}
	ElasticReadError  = &ArgError{"ONETECH", 409, 3, "ONETECH.40903004", "Ошибка считывания", "Недоступна база", "docs/ONETECH.40903004"}
	DeserializeBug    = &ArgError{"ONETECH", 415, 3, "ONETECH.41503001", "Ошибка сериализации", "Ошибка сериализации поискового движка", "docs/ONETECH.41503001"}

	// 500 series errors
	APIConnectError      = &ArgError{"ONETECH", 503, 3, "ONETECH.50303001", "Сервис API недоступен", "Ошибка соединения", "docs/ONETECH.50303001"}
	ScyllaDBConnectError = &ArgError{"ONETECH", 503, 3, "ONETECH.50303001", "ScyllaDB недоступна", "Ошибка соединения", "docs/ONETECH.50303001"}
	ElasticConnectError  = &ArgError{"ONETECH", 503, 3, "ONETECH.50303001", "Elastic недоступен", "Ошибка соединения", "docs/ONETECH.50303001"}
)

// ///////////////////////////////
// Returning error functions
// ///////////////////////////////
func (e *ArgError) Error() string {
	return fmt.Sprintf("%s %s", e.Code, e.DeveloperMessage)
}

func (e *ArgError) DevMessage(developMessage string) *ArgError {
	e.DeveloperMessage = developMessage
	return e
}

// ///////////////////////////////
// Error structure
// ///////////////////////////////
type ArgError struct {
	System           string `json:"system"`
	Status           int    `json:"status"`
	Series           int    `json:"series"`
	Code             string `json:"code"`
	Message          string `json:"message"`
	DeveloperMessage string `json:"developerMessage"`
	MoreInfo         string `json:"moreInfo"`
}
