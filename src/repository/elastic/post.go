package elastic

import (
	"context"
	"encoding/json"
	"github.com/olivere/elastic/v7"
	"oneportal-news/src/domain"
	"oneportal-news/src/errors"
)

type postCommandRepo struct {
	client *elastic.Client
}

func NewPostCommandRepo(client *elastic.Client) domain.PostCommandRepo {
	return &postCommandRepo{client}
}

func (r *postCommandRepo) Store(post *domain.Post) error {
	_, err := r.client.Index().
		Index("news").
		Id(post.Uid).
		BodyJson(post).
		Refresh("true").
		Do(context.TODO())
	return err
}

func (r *postCommandRepo) GetPostsSortedByCreatedOnAndAllPostsQty(size int, from int) (*[]domain.Post, int64, error) {
	query := elastic.NewMatchAllQuery()

	search, err := r.client.Search().
		Index("news").
		Query(query).
		Sort("created_on", false).
		From(from).
		Size(size).
		Do(context.TODO())
	if err != nil {
		return nil, 0, err
	}
	var posts []domain.Post
	allPostsQty := search.Hits.TotalHits.Value
	if allPostsQty > 0 {
		for _, hit := range search.Hits.Hits {
			var post domain.Post
			err := json.Unmarshal(hit.Source, &post)
			if err != nil {
				return nil, 0, err
			}
			posts = append(posts, post)
		}
	} else {
		errors.NoContentFound.DeveloperMessage = "No resource"
		return nil, 0, errors.NoContentFound
	}
	return &posts, allPostsQty, nil
}

func (r *postCommandRepo) Edit(post *domain.Post) error {
	_, err := r.client.Update().
		Index("news").
		Id(post.Uid).
		Doc(post).
		Refresh("true").
		Do(context.TODO())

	return err
}

func (r *postCommandRepo) GetByUid(uid string) (*domain.Post, error) {
	get, err := r.client.Get().
		Index("news").
		Id(uid).
		Do(context.TODO())

	if err != nil {
		errors.NoContentFound.DeveloperMessage = "No resource"
		return nil, errors.NoContentFound
	}

	var post domain.Post

	if get.Found {
		err := json.Unmarshal(get.Source, &post)
		if err != nil {
			return nil, err
		}
	}
	return &post, nil
}

func (r *postCommandRepo) DeleteByUid(uid string) error {
	_, err := r.client.Delete().
		Index("news").
		Id(uid).
		Do(context.TODO())
	if err != nil {
		return err
	}
	return err
}
