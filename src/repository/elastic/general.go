package elastic

import (
	"github.com/olivere/elastic/v7"
	"oneportal-news/src/config"
)

func ConnectionStart(configs *config.Configs) (*elastic.Client, error) {
	client, err := elastic.NewClient(
		elastic.SetURL(configs.Elastic.ConnectionUrl...),
		elastic.SetSniff(false),
		)
	if err != nil {
		return nil, err
	}

	return client, nil
}
