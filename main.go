package main

import (
	"flag"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"oneportal-news/src/config"
	"oneportal-news/src/news"
	"oneportal-news/src/repository/elastic"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func main() {
	// Set http port
	httpAddr := flag.String("http.addr", ":8080", "HTTP listen address only port :8080")
	flag.Parse()

	// Init logger
	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = level.NewFilter(logger, level.AllowAll())
	logger = &serializedLogger{Logger: logger}
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)

	// Get configs
	configs, err := config.GetConfigs()
	if err != nil {
		panic(err)
	}

	// Init connections
	elasticConnection, err := elastic.ConnectionStart(configs)
	if err != nil {
		panic(err)
	}

	postCommandRepo := elastic.NewPostCommandRepo(elasticConnection)

	fieldKeys := []string{"method"}

	// Init blog service
	newsService := news.NewService(postCommandRepo)
	newsService = news.NewLoggingService(log.With(logger, "component", "news"), newsService)
	newsService = news.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "oneportal",
			Subsystem: "news_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "oneportal",
			Subsystem: "news_service",
			Name:      "request_latency_microseconds",
			Help:      "Total duration of requests in microseconds.",
		}, fieldKeys),
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "oneportal",
			Subsystem: "news_service",
			Name:      "error_count",
			Help:      "Number of error requests received.",
		}, fieldKeys),
		newsService,
	)

	mux := http.NewServeMux()
	mux.Handle("/news/", news.MakeHandler(newsService))
	http.Handle("/news/", accessControl(mux))
	http.Handle("/metrics", promhttp.Handler())
	http.HandleFunc("/check", healthChecks)

	errs := make(chan error, 2)

	// Init http serve
	go func() {
		_ = logger.Log("transport", "http", "address", *httpAddr, "msg", "listening oneportal-news")
		errs <- http.ListenAndServe(*httpAddr, nil)
	}()

	// Listen errors chan
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	_ = logger.Log("terminated", <-errs)
}

// Additional structures & functions
type serializedLogger struct {
	mtx sync.Mutex
	log.Logger
}

func (l *serializedLogger) Log(keyvals ...interface{}) error {
	l.mtx.Lock()
	defer l.mtx.Unlock()
	return l.Logger.Log(keyvals...)
}

func healthChecks(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "ok")
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization,X-Owner,darvis-dialog-id")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
